import { Status } from './types';

export interface Question {
  id: string;
  type: QuestionType;
  text: string;
  categories: string[];
  isEditable: boolean;
  config: QuestionConfig;
  status: Status;
  createdAt: Date;
  updatedAt: Date;
}

export enum QuestionType {
  unique = 'unique',
  range = 'range',
  multiple = 'multiple',
  matrixUnique = 'matrixUnique',
}

type QuestionConfig =
  | {
      minText: string;
      maxText: string;
    }
  | { options: string[] }
  | { options: string[]; items: string[] };
