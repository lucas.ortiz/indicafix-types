export interface Questionnaire {
  id: string;
  versionName: string;
  surveyId: string;
  title: string;
  openingDescription: string;
  inputs: string[];
  questions: string[];
  index: number;
  createdAt: Date;
  updatedAt: Date;
}
