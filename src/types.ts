export enum Status {
  active = 'active',
  inactive = 'inactive',
  archived = 'archived',
}
