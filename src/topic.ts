import { Status } from './types';

export interface Topic {
  id: string;
  slug: string;
  name: string;
  title: string;
  description: string;
  openingDescription: string;
  inputs: string[];
  questions: string[];
  index: number;
  createdAt: string;
  updatedAt: Date;
  status: Status;
}
