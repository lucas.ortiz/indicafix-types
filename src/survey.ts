import { Status } from './types';

export interface Survey {
  id: string;
  uuid: string;
  name: string;
  participants: number;
  universe: number;
  companyId: string;
  dashboardUrl: string;
  activeQuestionnaire: string;
  rounds: ReadonlyArray<Round>;
  status: Status;
  createdAt: Date;
  updatedAt: Date;
}

export enum RoundStatus {
  started = 'started',
  paused = 'paused',
  finished = 'finished',
  pending = 'pending',
}

export interface Round {
  id: string;
  startAt: Date;
  finishAt: Date;
  dashboardUrl: string;
  status: RoundStatus;
}
